const Twit = require('twit')
const Promise = require('bluebird')
const DeepstreamServer = require('deepstream.io')
const deepstream = require('deepstream.io-client-js')
const config = require('./config.json')
const server = new DeepstreamServer({
  host: 'localhost',
  port: 6020
})

const T = new Twit({
  consumer_key:         '',
  consumer_secret:      '',
  access_token:         '',
  access_token_secret:  ''
})

server.on('started', function() {
  const ds = deepstream('localhost:6020').login()

  let getScoresFor = function(item, list) {
    return new Promise(function(resolve, reject) {
      let cnt = 0;
      let url = `?q=%23${item.tag}%20since%3A${config.start_date}&count=100`

      let get = function(url) {
        console.log('get', url)
        T.get(`search/tweets.json${url}`, function(err, data, response) {
          cnt += data.statuses.length

          if (data.search_metadata.next_results) {
            get(data.search_metadata.next_results)
          } else {
            let record = ds.record.getRecord(item.tag)
            record.whenReady(record => {
              item.count = cnt
              record.set(item)
              list.addEntry(item.tag)
              console.log(item)
              resolve()
            })
          }
        })
      }

      get(url)
    })
  }

  const ds_list = ds.record.getList('items')
  const promises = []

  ds_list.whenReady(function() {
    // Get actual scores ...
    config.items.map(function(item) {
      promises.push(getScoresFor(item, ds_list))
    })

    Promise.all(promises).then(function() {

      console.log('Now listen in live')

      let tracks = []
      config.items.map(function(i) {
        tracks.push(`#${i.tag}`)
      })

      console.log(tracks)

      let stream = T.stream('statuses/filter', { track: tracks })

      stream.on('tweet', function(tweet) {
        config.items.map(function(i) {
          if (tweet.text.toLowerCase().search(`#${i.tag}`) > -1) {
            let record = ds.record.getRecord(i.tag)
            i.count += 1
            console.log(i.tag, '+1')
            record.set(i)
          }
        })
      })
    })
  })
})

server.start()
